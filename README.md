# README #

Fixlog.py removes bad tpicd#tpcont measurements from Field System (fs) logs.

Created for L-band receiver on radio telescope RT32 in Torun Institute of Astronomy, Nicolaus Copernicus University.

### How do I get set up? ###

#### Libraries ####

##### Needed #####
* only standard python library

#### Running ####
* python fixlog.py experimentname.log


### Who do I talk to? ###
* Rafał Sarniak
* kain at astro.umk.pl
 
