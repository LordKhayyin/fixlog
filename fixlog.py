#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Rafal Sarniak

import os, sys
import argparse

def fixlog(filepath):
    with open(filepath, "r" ) as file:
        lines = file.readlines()
    dates_to_remove = []
    for i in range(len(lines)):
        #if "$$$$$,$$$$$," in lines[i]:
        if "$$$$$" in lines[i]:
            dates_to_remove.append( lines[i][:20] )
            #print(dates_to_remove[-1])
    for date in dates_to_remove:
        # first pass
        for line in lines:
            if (date in line) and ("tpicd#tpco" in line):
                print("Removing:", line[:-1])
                lines.remove(line)
        # second pass
        for line in lines:
            if (date in line) and ("tpicd#tpco" in line):
                print("Removing:", line[:-1])
                lines.remove(line)
        # third pass
        for line in lines:
            if (date in line) and ("tpicd#tpco" in line):
                print("Removing:", line[:-1])
                lines.remove(line)
    # Jeszcze trzeba usunąć rzeczy    
    dates_to_remove = []
    for i in range(len(lines)):
        if "$$$$$" in lines[i]:
            dates_to_remove.append( lines[i][:20] )
            #print(dates_to_remove[-1])
    for date in dates_to_remove:
        # first pass
        for line in lines:
            if (date in line) and ("tpicd#tsys" in line):
                print("Removing:", line[:-1])
                lines.remove(line)
        # second pass
        for line in lines:
            if (date in line) and ("tpicd#tsys" in line):
                print("Removing:", line[:-1])
                lines.remove(line)
        
        
    with open(filepath + ".fixed", 'w') as file:
        for line in lines:
           file.write(line)
        print("\nFixed file saved in: " + filepath + ".fixed\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Fixes $$$$$,$$$$$, problem in log (removing pairs)')
    parser.add_argument('filepath', help='filepath of file to fix')
    args = parser.parse_args()
    fixlog(args.filepath) 
